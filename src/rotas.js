import React from "react";
import {HashRouter, Switch, Route} from 'react-router-dom';

import Home from "./views/home";
import CadastroProduto from "./views/produtos/cadastro";
import ConsultaProdutos from "./views/produtos/consulta";

export default () => {
    return (        
        <Switch>
            <Route path="/cadastro-produtos/:sku?" component={CadastroProduto} />
            <Route path="/consulta-produtos" component={ConsultaProdutos}/>
            <Route path="/" component={Home} />
        </Switch>       
    )
}